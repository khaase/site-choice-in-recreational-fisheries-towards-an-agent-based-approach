import java.io.FileReader

import org.apache.commons.csv.CSVFormat
import org.apache.commons.math3.random.RandomGenerator
import org.jamesii.ml3.experiment.init.IInitialStateBuilder
import org.jamesii.ml3.model.agents.{AgentDeclaration, IAgent, IAgentFactory}
import org.jamesii.ml3.model.state.{IState, IStateFactory}
import org.jamesii.ml3.model.values._
import org.jamesii.ml3.model.{Model, Parameters}

import scala.collection.JavaConverters


object M_D1_Experiment extends App {
  implicit def string2value(v: String): IValue = new StringValue(v)

  implicit def int2value(v: Int): IValue = new IntValue(v)

  implicit def real2value(v: Double): IValue = new RealValue(v)

  implicit def bool2value(v: Boolean): IValue = new BoolValue(v)

  import sessl._
  import sessl.ml3._

  execute {
    new Experiment with ParallelExecution with Observation with CSVOutput with ParameterMaps {

      model = "M_D1.ml3"
      simulator = NextReactionMethod()

      parallelThreads = -1
      replications = 1

      stopTime = 365

      initializeWith(() => InitialState)

      fromFile("distance_short.csv")()
      set("trip_rate" <~ 9.0 / 365)

      //csvOutputDirectory(() => s"results")

      //observeAt(range(0, 10, stopTime))
      //observe("FishingLocation" ~ expressionDistribution("FishingLocation", "ego.name"))
      //observe("trips" ~ expressionDistribution("FishingLocation", "ego.trips"))
      //observe("n_boot" ~ agentCount("Angler", "ego.fishing_method = 'Boot'"))
      //observe("n_kutter" ~ agentCount("Angler", "ego.fishing_method = 'Kutter'"))
      //observe("n_land" ~ agentCount("Angler", "ego.fishing_method = 'Land'"))

      val trip = Change("Angler", "n_fishingdays")
      observe("ID" ~ (expression("ego.id") at trip))
      observe("Angler_fishing_method" ~ (expression("ego.fishing_method") at trip))
      observe("Angler_postcode" ~ (expression("ego.postcode") at trip))
      observe("Angler_max_distance" ~ (expression("ego.max_distance") at trip))
      observe("FishingLocation" ~ (expression("ego.last_fishing_location") at trip))
      observe("FishingLocation_method" ~ (expression("ego.last_fishing_location_method") at trip))


      withRunResult(writeCSV)

      object InitialState extends IInitialStateBuilder {
        override def buildInitialState(model: Model, sf: IStateFactory, af: IAgentFactory, rng: RandomGenerator, par: Parameters): IState = {
          val s = sf.create()
          val n_angler = 10000
          var id = 0

          val angler = model.getAgentDeclaration("Angler")
          val location = model.getAgentDeclaration("FishingLocation")
          val knowledge = model.getAgentDeclaration("LocationKnowledge")

          val locations_file = new FileReader("average_catch.csv")
          val location_records = CSVFormat.newFormat(',').withFirstRecordAsHeader().parse(locations_file)
          for (record <- JavaConverters.iterableAsScalaIterable(location_records)) {
            val name = record.get("\"fishing_location\"").replace("\"","")
            val method = record.get("\"fishing_method\"").replace("\"","")
            val average_catch = record.get("\"average_catch\"").toDouble
            val l = createFishingLocation(location, af, name, method, average_catch)
            s.addAgent(l)
          }

          val postcode_file = new FileReader("angler_per_zipcode.csv")
          val postcode_records = CSVFormat.newFormat(',').withFirstRecordAsHeader().parse(postcode_file).getRecords
          for (i <- 1 to n_angler) {
            val r = rng.nextDouble()
            val iterator = postcode_records.iterator()
            var record = iterator.next()
            var sum = record.get("\"perc_zipcode\"").toDouble
            while (sum < r) {
              record = iterator.next()
              sum += record.get("\"perc_zipcode\"").toDouble
            }
            val postcode = record.get("\"zip_code\"")
            val max_distance = record.get("\"max_distance\"").toDouble
            val method = record.get("\"fishing_method\"").replace("\"","")
            val a = createAngler(id, postcode, max_distance, method, angler, rng, af)
            id += 1
            s.addAgent(a)
          }

          val agents = s.getAgentsByType("Angler").toArray
          val locations = s.getAgentsByType("FishingLocation").toArray
          for (i <- 0 until agents.length) {
            for (j <- 0 until locations.length) {
              val a = agents(i).asInstanceOf[IAgent]
              val l = locations(j).asInstanceOf[IAgent]
              val a_method = a.getAttributeValue("fishing_method").asInstanceOf[StringValue].getValue
              val l_method = l.getAttributeValue("fishing_method").asInstanceOf[StringValue].getValue
              if (a_method == l_method) {
                val average_catch = l.getAttributeValue("average_catch").asInstanceOf[RealValue].getDouble
                val k = createLocationKnowledge(knowledge, af, a, l, average_catch)
                s.addAgent(k)
              }
            }
          }

        return s
      }

    }

  }
}

def truncatedNormal(mean: Double, sigma: Double, min: Double, max: Double, rng: RandomGenerator): Double = {
  var n = sigma * rng.nextGaussian() + mean
  while (n < min || n > max) {
    n = sigma * rng.nextGaussian() + mean
  }
  return n
}

def createAngler(id: Int, postcode: String, max_distance: Double, method : String, typ: AgentDeclaration, rng: RandomGenerator, af: IAgentFactory): IAgent = {
  val a = af.createAgent(typ, 0)
  a.setAttributeValue("id", id)
  a.setAttributeValue("postcode", postcode)
  a.setAttributeValue("max_distance", max_distance)
  a.setAttributeValue("centrality", rng.nextDouble())
  a.setAttributeValue("age2", truncatedNormal(45.34, 15.5, 14, 90, rng))
  a.setAttributeValue("fishing_method", method)
  return a
}

def createFishingLocation(typ: AgentDeclaration, af: IAgentFactory, name: String, method : String, average_catch: Double): IAgent = {
  val a = af.createAgent(typ, 0)
  a.setAttributeValue("name", name)
  a.setAttributeValue("average_catch", average_catch)
  a.setAttributeValue("fishing_method", method)
  return a
}

def createLocationKnowledge(typ: AgentDeclaration, af: IAgentFactory, owner: IAgent, target: IAgent, average_catch: Double): IAgent = {
  val a = af.createAgent(typ, 0)
  a.setAttributeValue("average_catch", average_catch)

  a.addLink("owner", owner)
  owner.addLink("knowledge", a)

  a.addLink("target", target)
  target.addLink("knowledge_about", a)

  return a
}

}


