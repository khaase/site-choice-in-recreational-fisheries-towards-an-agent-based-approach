import java.io.FileReader

import org.apache.commons.csv.{CSVFormat, CSVRecord}
import org.apache.commons.math3.random.RandomGenerator
import org.jamesii.ml3.experiment.init.IInitialStateBuilder
import org.jamesii.ml3.model.agents.{AgentDeclaration, IAgent, IAgentFactory}
import org.jamesii.ml3.model.state.{IState, IStateFactory}
import org.jamesii.ml3.model.values._
import org.jamesii.ml3.model.{Model, Parameters}
import travel_model_old_data_Experiment.{createAngler, createFishingLocation, truncatedNormal}


object M_C2a_Experiment extends App {
  implicit def string2value(v: String): IValue = new StringValue(v)

  implicit def int2value(v: Int): IValue = new IntValue(v)

  implicit def real2value(v: Double): IValue = new RealValue(v)

  implicit def bool2value(v: Boolean): IValue = new BoolValue(v)

  import sessl._
  import sessl.ml3._

  import scala.collection.JavaConverters

  execute {
    new Experiment with ParallelExecution with Observation with CSVOutput with ParameterMaps {

      model = "M_C2a.ml3"
      simulator = NextReactionMethod()

      parallelThreads = -1
      replications = 1

      stopTime = 365

      initializeWith(() => InitialState)

      fromFile("postcode-distance-test.csv")()
      set("trip_rate" <~ 9.0/365)

      //csvOutputDirectory(() => s"results")
      //observeAt(range(0, 10, stopTime))
      //observe("trips" ~ expressionDistribution("FishingLocation", "ego.trips"))

      val trip = Change("Angler", "n_fishingdays")
      observe("ID" ~ (expression("ego.id") at trip))
      observe("Angler_fishing_method" ~ (expression("ego.fishing_method") at trip))
      observe("Angler_postcode" ~ (expression("ego.postcode") at trip))
      observe("FishingLocation" ~ (expression("ego.last_fishing_location") at trip))
      observe("FishingLocation_method" ~ (expression("ego.last_fishing_location_method") at trip))

      withRunResult(writeCSV)

      object InitialState extends IInitialStateBuilder {
        override def buildInitialState(model: Model, sf: IStateFactory, af: IAgentFactory, rng: RandomGenerator, par: Parameters): IState = {
          val s = sf.create()
          val n_angler = 10000
          var id = 0

          val angler = model.getAgentDeclaration("Angler")
          val location = model.getAgentDeclaration("FishingLocation")

          //val p1 = createPostarea(postarea, af, "1")
          //val p2 = createPostarea(postarea, af, "2")

          //val l1 = createFishingLocation(location, af, "l1", "boat", 5)
          //val l2 = createFishingLocation(location, af, "l2", "boat", 10)
          //val l3 = createFishingLocation(location, af, "l3", "boat", 30)
          //val l4 = createFishingLocation(location, af, "l4", "land", 5)

          val locations_file = new FileReader("average_catch.csv")
          val location_records = CSVFormat.newFormat(',').withFirstRecordAsHeader().parse(locations_file)
          for (record <- JavaConverters.iterableAsScalaIterable(location_records)) {
            val name = record.get("\"fishing_location\"").replace("\"","")
            val method = record.get("\"fishing_method\"").replace("\"","")
            val average_catch = record.get("\"average_catch\"").toDouble
            val l = createFishingLocation(location, af, name, method, average_catch)
            s.addAgent(l)
          }

          //s.addAgent(p1);
          //s.addAgent(p2);
          //s.addAgent(l1);
          //s.addAgent(l2);
          //s.addAgent(l3);
          //s.addAgent(l4);


          //for (i <- 1 to 10) {
          //val a = createAngler(id, "boat", p1, angler, rng, af)
          //s.addAgent(a)
          //id += 1
          //}
          //for (i <- 1 to 10) {
          //val a = createAngler(id, "boat", p2, angler, rng, af)
          //s.addAgent(a)
          // id += 1
          //}
          //for (i <- 1 to 10) {
          // val a = createAngler(id, "land", p1, angler, rng, af)
          // s.addAgent(a)
          // id += 1
          //}

          val postcode_file = new FileReader("angler_per_zipcode.csv")
          val postcode_records = CSVFormat.newFormat(',').withFirstRecordAsHeader().parse(postcode_file).getRecords
          for (i <- 1 to n_angler) {
            val r = rng.nextDouble()
            val iterator = postcode_records.iterator()
            var record = iterator.next()
            var sum = record.get("\"perc_zipcode\"").toDouble
            while (sum < r) {
              record = iterator.next()
              sum += record.get("\"perc_zipcode\"").toDouble
            }
            val postcode = record.get("\"zip_code\"")
            val method = record.get("\"fishing_method\"").replace("\"","")
            val a = createAngler(id, postcode, method, angler, rng, af)
            id += 1
            s.addAgent(a)
          }

          return s
        }
      }

    }
  }

  def truncatedNormal(mean: Double, sigma: Double, min: Double, max: Double, rng: RandomGenerator): Double = {
    var n = sigma * rng.nextGaussian() + mean
    while (n < min || n > max) {
      n = sigma * rng.nextGaussian() + mean
    }
    return n
  }

  def createAngler(id: Int, postcode: String, method : String, typ: AgentDeclaration, rng: RandomGenerator, af: IAgentFactory): IAgent = {
    val a = af.createAgent(typ, 0)
    a.setAttributeValue("id", id)
    a.setAttributeValue("postcode", postcode)
    a.setAttributeValue("centrality", rng.nextDouble())
    a.setAttributeValue("age2", truncatedNormal(45.34, 15.5, 14, 90, rng))
    a.setAttributeValue("fishing_method", method)
    return a
  }


  def createFishingLocation(typ: AgentDeclaration, af: IAgentFactory, name : String, method : String, average_catch : Double): IAgent = {
    val a = af.createAgent(typ, 0)
    a.setAttributeValue("name", name)
    a.setAttributeValue("fishing_method", method)
    a.setAttributeValue("average_catch", average_catch)
    return a
  }

}


