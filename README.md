# Site Choice in Recreational Fisheries – Towards an Agent-Based Approach #

In this repository you find the files corresponding to the publication "Site Choice in Recreational Fisheries – Towards an Agent-Based Approach" for the Winter Simulation Conference 2022.
The repository include all models with the corresponding experiments.
The data to set up the models ("angler_per_zipcode", "average_catch", "average_harvest", and "distance_short") and the data to compare the model output to the real world ("real world data") are located in the main repository.
The analysis to compare the model outputs with the real world data are done in R. The corresponding R-Project and skripts are located in the subrepository "R-code".

## Software requirements

The following software is needed to execute the models and anaylsis:
- Java Runtime Environment 8 or newer (e.g., from https://adoptopenjdk.net/).
- R, required packages are shown at the beginning of each script.

All other required software will be downloaded automatically when the experiment is executed the first time.

## Further Reading about ML3

The Modeling Language for Linked Lives (ML3) is a domain-specific modeling language for agent based models in the social sciences. An informal description of the language can be found in [1]. A formal definition of the language and its semantics can be found in [2]. The models provided with [3] may be of use as further examples. Please not that there have been slight changes to the syntax over the year, so please compare with most recent version in this repository or in [2] if there are any issues.

## Further Reading and Documentation about SESSL

Simulation Experiment Specification via a Scala Layer (SESSL) is a domain-specific language for the specification of simulation experiments [4]. The SESSL-ML3 binding allows for the use of SESSL together with ML3 [3]. For documentation (including a User Guide and a Developer Guide), see [sessl.org](http://sessl.org/).

## References

[1] Tom Warnke, Oliver Reinhardt, Anna Klabunde, Frans Willekens, Adelinde M. Uhrmacher. "Modelling and simulating decision processes of linked lives: An approach based on concurrent processes and stochastic race" In: Population Studies, 71 (sup1) 69-83, 2017.

[2] Oliver Reinhardt, Tom Warnke, Adelinde M. Uhrmacher. "A Language for Agent-Based Discrete-Event Modeling and Simulation of Linked Lives" In: ACM Transactions on Modeling and Computer Simulation (under review).

[3] Oliver Reinhardt, Jason Hilton, Tom Warnke, Jabuk Bijak, Adelinde M. Uhrmacher: "Streamlining Simulation Experiments with Agent-Based Models in Demography" In: Journal of Artificial Societies and Social Simulation, 21 (3) 9, 2018.

[4] Roland Ewald and Adelinde M. Uhrmacher. "SESSL: A Domain-Specific Language for Simulation Experiments". In: ACM Transactions on Modeling and Computer Simulation 24(2), 2014.
