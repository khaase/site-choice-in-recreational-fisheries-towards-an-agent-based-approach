////////////////////////////////////////////////////////     Agents
Angler(
  id : int,                          // identifies the angler
  age2 : real,                       // actual age of an angler
  centrality : real,                 // centrality from fishing to life for the angler
  fishing_method : string,            // the fishing method an angler can do
  postcode : string,                 // home location of the angler
  max_distance : real,               // max distance angler drives from home location
  on_trip : bool := false,           // identifies if the angler is on a trip or not (in this model always false)
  n_fishingdays : int,               // total number of fishingdays from the angler
  last_catch : int,                  // number of caught fish at the last fishingday
  last_fishing_location : string,   // name of the last fishing location
  last_fishing_location_method : string,   // name of the last fishing location method
  time_last_catch : real,            // time of the last catch
  has_current_catch : bool := false, // identifies if the last catch was in the last 14 days
  total_catch : int,                 // total number of caught fish
  max_catch : int := 3               // maximum catch the angler has heard of (own, network or broadcast)
)



FishingLocation(
  name : string,
  fishing_method : string,
  average_catch : real,
  trips : int
)

LocationKnowledge(
  average_catch : real
)


trip_rate : real

distance[string] : real

// expectedcatch_intercept : real
// expectedcatch_b : real
// distance_intercept : real
// distance_b : real

//////////////////////////////////////////////////////////   Links between Agents
owner:Angler[1]<->[n]LocationKnowledge:knowledge
target:FishingLocation[1]<->[n]LocationKnowledge:knowledge_about
network:Angler[n]<->[n]Angler:network
visits:Angler[n]<->[n]FishingLocation:visitors

////////////////////////////////////////////////////////// Functions
/*
////////// site choice Level 1
// angler go to the spot with the highest average catch
// idea: after years of angling angler know where the catches are best,
// Theory: basically rational choice/perfect knowledge
// will probably not recreate the travel pattern -> angler chose the spot not rational or don´t have perfect knowledge
Angler.decide_site_location() : FishingLocation :=
  FishingLocation.all.argmax(alter.average_catch)


////////// site choice Level 2
// angler go to the spot with the highest average catch of the spots they know
// idea: after years of angling angler know where the catches are best, but not for the whole coast
// Theory: basically bounded rational choice/imperfect knowledge
// will probably better recreate the travel pattern at least the spread along the coast ->
// but with a random knowledge some will drive from east to west and vise versa, which not reflects thr travel pattern
// However angler seem to chose the spot with imperfect knowledge, but which spot do they know?
Angler.decide_site_location() : FishingLocation :=
  if ego.knowledge.size() > 0 then ego.knowledge.argmax(alter.average_catch).target else FishingLocation.all.argmax(alter.average_catch)
// add additional link to identify known locations

*/

////////// site choice Level 3
// angler go to the spot with the highest average catch of the spots they know, spot they know are restricted due to the travel distance
// idea: after years of angling angler know where the catches are best, but only for spots that are within there driving range
// Theory: basically bounded rational choice/imperfect knowledge
// will probably better recreate the travel pattern  ->
// max travel distance seems to build a range in which angler chose a spot
Angler.decide_site_location() : FishingLocation :=
    ego.knowledge.argmin(distance[concat(ego.postcode, "-", alter.target.name)]).target


//    if ?filtered.size() > 0 then
//         ?filtered.argmax(alter.average_catch).target
//     else if ?filtered2.size() > 0 then
//         ?filtered2.argmax(alter.average_catch)
//     else FishingLocation.all.argmax(alter.average_catch)
// where ?filtered := ego.knowledge.filter(distance[concat(ego.postcode, "-", alter.target.name)] <= ego.max_distance)
//       ?filtered2 := FishingLocation.all.filter(distance[concat(ego.postcode, "-", alter.name)] <= ego.max_distance)

/*
////////// site choice Level 4
// angler go to the spot with the highest average catch of the spots within there travel distance,
// but also prefer shorter distances within there travel range
// Theory:  basically utility as combination from catch and distance
// will probably better recreate the travel pattern  ->
// max travel distance seems to build a range in which angler prefer shorter distances
Angler.decide_site_location(?when : real) : FishingLocation :=

    ego.fishing_locations.max(?utility)
    where ?utility =: ego.expected_catch_utility(now, ...) + ego.distance_utility(...)
// linear function of expected catch - utility
// expected catch from the list of spots generated from data
Angler.expected_catch_utility(?when : real, ?location : FishingLocation) : real :=
    expectedcatch.intercept + expectedcatch * expectedcatch.b
// negative linear function of distance and utility
// have to be calculated for each PostArea - FishingLocation combination
Angler.distance_utility(?location : FishingLocation) : real :=
  // distance to that location: ego.home.links.filter(alter.fishinglocation = ?location).only().distance
   distance.intercept - distance * distance.b


////////// site choice Level 5
// same as level 4, but angler build there own catch through experience (or friends) not from the overall data
// idea: knowledge of catches is more restricted
Angler.decide_site_location(?when : real) : FishingLocation :=
    ego.fishing_locations.max(?utility)
    where ?utility =: ego.expected_catch_utility + ego.distance_utility
// linear function of expected catch - utility
// expected catch from memory (and friends)
Angler.expected_catch_utility(?when : real, ?location : FishingLocation) : real :=
    expectedcatch.intercept + expectedcatch * expectedcatch.b
// negative linear function of distance and utility
// have to be calculated for each PostArea - FishingLocation combination
Angler.distance_utility : real :=
   distance.intercept - distance * distance.b


////////// Catch

// real catch rate of angler
// normal distribution around the average catch at a location from data
Angler.catch(?when : real, ?location : FishingLocation) : int :=
  round(max(0,normal(FishingLocation.average_catch, 1)))


//// final function

*/

// put the  functions above in one fishing function
Angler.fishing() ->
   ?location := ego.decide_site_location()
   //?catch := ego.catch(now, ?location)
   ego.n_fishingdays += 1
   ego.last_fishing_location := ?location.name
   ego.last_fishing_location_method := ?location.fishing_method
   ?location.trips += 1
   //ego.last_catch := ?catch
   //ego.total_catch += ?catch





////////////////////////////////////////////////////////////    Rules

////////////////////////////////////////  site choice

Angler
| true // !ego.on_trip
@ trip_rate
-> ego.fishing()


