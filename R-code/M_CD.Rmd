---
title: "level4_tester"
author: "Kevin Haase"
date: "16 3 2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r packages, message=FALSE, warning=FALSE, include=FALSE}
#Markdown
library("citr")
library("captioner")
library("png")

#data work
library("readr")
#library("plyr")
library("dplyr")
library("tidyr")
library("reshape2")
library("stringr")
#library("Rmisc")

#plots
library("ggplot2")
library("ggrepel")
library("cowplot")
#library("ggpubr")

#maps
#library("OpenStreetMap")
library("maps")
library("mapdata")
library("maptools")

#clustern
library("cluster")
library("factoextra")
library("clustertend")
library("dendextend")
library("NbClust")
library("fpc")
library("MASS")
library("vegan")

#SQL
library("DBI")
library("RMySQL")
```




#Import datasets

Dataset of the experiment
```{r}
run_0 <- read_csv("ML3/lv6_data/config-0/run-0.csv")

run <- subset(x = run_0, select = c(ID, Angler_fishing_method, Angler_postcode, Angler_max_distance, FishingLocation, FishingLocation_method))

run_safe <- run
```

Calibration dataset
```{r}
run <- run_safe
dat <- dat_safe
dat <- filter(.data = dat, zip_code > 9999 & zip_code < 33000 | 
                            zip_code > 37999 & zip_code < 40000 |
                            zip_code > 48999 & zip_code < 50000)
```





# angler characteristics

## Number of angler (per survey category)

```{r}
length(unique(run$ID))

n <- length(unique(run$ID))
run %>%
  group_by(Angler_fishing_method)%>%
  summarise(perc.angler = length(unique(ID))/n)

length(dat$ID)

n <- length(dat$ID)
dat %>%
  group_by(survey_category)%>%
  summarise(perc.angler = length(unique(ID))/n)
```

## Origin of angler

```{r}
range(run$Angler_postcode)

range(dat$zip_code)
```






#Fishing method

Are all angler driving to locations with the same fishing method?
```{r}
run$FishingLocation[run$FishingLocation_method != run$Angler_fishing_method]
```



# Fishing locations

anzahl fishing locations

```{r}
length(unique(run$FishingLocation))
length(unique(dat$harbour_code))

run %>%
  group_by(Angler_fishing_method)%>%
  summarise(n.locations = length(unique(FishingLocation)))

dat %>%
  group_by(survey_category)%>%
  summarise(n.locations = length(unique(harbour_code)))

a <- unique(run$FishingLocation[run$FishingLocation_method == "Kutter"])
b <- unique(dat$harbour_code[dat$survey_category == "Kutter"])

#all locations that are in the experiment (a) and in data (b)
a[a %in% b]
#all locations that are in the experiment (a) but not in data (b)
a[!(a %in% b)]
#all locations that are in the data (b) but not in the experiment (a)  
b[!(b %in% a)]
```



distribution of angler between the fishing locations

in total
```{r}
run <- run_safe
dat <- dat_safe
dat <- filter(.data = dat, zip_code > 9999 & zip_code < 33000 | 
                            zip_code > 37999 & zip_code < 40000 |
                            zip_code > 48999 & zip_code < 50000)
```

```{r}
#in the experiment
run <- mutate(.data = run, n.total.fishingday.exp = length(ID))
run <- run %>%
  group_by(n.total.fishingday.exp, FishingLocation)%>%
  summarise(n.fishingday.exp = length(ID))
run <- mutate(.data = run, perc_exp = n.fishingday.exp/n.total.fishingday.exp)

model_lv6 <- run

#in the data
dat <- mutate(.data = dat, n.total.fishingday = length(ID))
dat <- dat %>%
  group_by(harbour_code, n.total.fishingday) %>%
  summarise(n.fishingday.harbour = length(ID))
dat <- mutate(.data = dat, perc_data = n.fishingday.harbour/n.total.fishingday)


names(run)[names(run) == "FishingLocation"] <- "harbour_code"
test <- left_join(dat, run)

#refill possible Na´s
test$n.total.fishingday.exp <- unique(test$n.total.fishingday.exp[is.na(test$n.total.fishingday.exp) == F]) 
test$n.fishingday.exp[is.na(test$n.fishingday.exp) == T] <- 0
test$perc_exp[is.na(test$perc_exp) == T] <- 0

mean(test$perc_exp)
sd(test$perc_exp)
median(test$perc_exp)
range(test$perc_exp)

test$diff <- test$perc_data - test$perc_exp
mean(abs(test$diff), na.rm = T)
```



distribution of angler between the fishing locations

per fishing method
```{r}
run <- run_safe
dat <- dat_safe
dat <- filter(.data = dat, zip_code > 9999 & zip_code < 33000 | 
                            zip_code > 37999 & zip_code < 40000 |
                            zip_code > 48999 & zip_code < 50000)
```

```{r}
#in the experiment
run <- run %>%
  group_by(Angler_fishing_method)%>%
  mutate(n.total.fishingday.exp = length(ID))
run <- run %>%
  group_by(Angler_fishing_method, n.total.fishingday.exp, FishingLocation)%>%
  summarise(n.fishingday.exp = length(ID))
run <- mutate(.data = run, perc_exp = n.fishingday.exp/n.total.fishingday.exp)


#in the data
dat <- dat %>%
  group_by(survey_category)%>%
  mutate(n.total.fishingday = length(ID))
dat <- dat %>%
  group_by(survey_category, harbour_code, n.total.fishingday) %>%
  summarise(n.fishingday.harbour = length(ID))
dat <- mutate(.data = dat, perc_data = n.fishingday.harbour/n.total.fishingday)


names(run)[names(run) == "Angler_fishing_method"] <- "survey_category"
names(run)[names(run) == "FishingLocation"] <- "harbour_code"
test <- left_join(dat, run)

#refill possible Na´s
test$n.total.fishingday.exp[test$survey_category == "Boot"] <- unique(test$n.total.fishingday.exp[test$survey_category == "Boot" & is.na(test$n.total.fishingday.exp) == F])
test$n.total.fishingday.exp[test$survey_category == "Kutter"] <- unique(test$n.total.fishingday.exp[test$survey_category == "Kutter" & is.na(test$n.total.fishingday.exp) == F])
test$n.total.fishingday.exp[test$survey_category == "Land"] <- unique(test$n.total.fishingday.exp[test$survey_category == "Land" & is.na(test$n.total.fishingday.exp) == F])
test$n.fishingday.exp[is.na(test$n.fishingday.exp) == T] <- 0
test$perc_exp[is.na(test$perc_exp) == T] <- 0

test%>%
  group_by(survey_category)%>%
  summarise(mean.perc_exp = mean(perc_exp)*100,
            median.perc_exp = median(perc_exp)*100,
            sd.perc_exp = sd(perc_exp)*100,
            min.perc_exp = min(perc_exp)*100,
            max.perc_exp = max(perc_exp)*100)


test$diff <- test$perc_data - test$perc_exp
names(average_catch)[names(average_catch) == "fishing_method"] <- "survey_category"
names(average_catch)[names(average_catch) == "fishing_location"] <- "harbour_code"
test <- left_join(test, average_catch)

test%>%
  group_by(survey_category)%>%
  summarise(mean.diff = mean(abs(diff), na.rm = T))
```











# range of distances per 2 digit zip code

## pooled over fishing methods

```{r}
run <- run_safe
dat <- dat_safe
dat <- filter(.data = dat, zip_code > 9999 & zip_code < 33000 | 
                            zip_code > 37999 & zip_code < 40000 |
                            zip_code > 48999 & zip_code < 50000)

#transform 5 digit zipcode to 2 digit
run$zip_code <- formatC(run$Angler_postcode, width = 5, format = "d", flag = "0")
run$zip_code <- as.numeric(substr(run$zip_code, 1, 2))

dat$zip_code <- formatC(dat$zip_code, width = 5, format = "d", flag = "0")
dat$zip_code <- as.numeric(substr(dat$zip_code, 1, 2))

names(run)[names(run) == "FishingLocation"] <- "ap_code"
names(dat)[names(dat) == "harbour_code"] <- "ap_code"
```

work on distance data
```{r}
dist <- dist_safe

names(dist)[names(dist) == "zipcode"] <- "zip_code"

#transform 5 digit zipcode to 2 digit
dist$zip_code <- formatC(dist$zip_code, width = 5, format = "d", flag = "0")
dist$zip_code <- as.numeric(substr(dist$zip_code, 1, 2))

dist <- subset(x = dist, select = c(ap_code, zip_code, distance_km))
```



combine both datasets with distances
```{r}
run <- left_join(run, dist)

dat <- left_join(dat, dist)
```


calculate travel distances per zip code
```{r}
run <- run %>%
  group_by(zip_code) %>%
  summarise(min.dist.e = min(distance_km),
            max.dist.e = max(distance_km),
            mean.dist.e = mean(distance_km))
run <- mutate(.data = run, diff.e = max.dist.e - min.dist.e)

dat <- dat %>%
  group_by(zip_code) %>%
  summarise(min.dist = min(distance_km),
            max.dist = max(distance_km),
            mean.dist = mean(distance_km))
dat <- mutate(.data = dat, diff = max.dist - min.dist)
```

```{r}
test <- left_join(run, dat)

test <- mutate(.data = test, min.diff = min.dist - min.dist.e,
                              max.diff = max.dist - max.dist.e,
                              mean.diff = mean.dist - mean.dist.e,
                              diff.diff = diff - diff.e)

mean(abs(test$mean.diff))
```

```{r}
ggplot()+
  geom_pointrange(data = dat, aes(x = mean.dist, y = zip_code, xmin = min.dist, xmax = max.dist))+
  geom_pointrange(data = run, aes(x = mean.dist.e, y = zip_code+0.5, xmin = min.dist.e, xmax = max.dist.e), col = "red")
```









## for each fishing methods

```{r}
run <- run_safe
dat <- dat_safe
dat <- filter(.data = dat, zip_code > 9999 & zip_code < 33000 | 
                            zip_code > 37999 & zip_code < 40000 |
                            zip_code > 48999 & zip_code < 50000)

#transform 5 digit zipcode to 2 digit
run$zip_code <- formatC(run$Angler_postcode, width = 5, format = "d", flag = "0")
run$zip_code <- as.numeric(substr(run$zip_code, 1, 2))

dat$zip_code <- formatC(dat$zip_code, width = 5, format = "d", flag = "0")
dat$zip_code <- as.numeric(substr(dat$zip_code, 1, 2))

names(run)[names(run) == "FishingLocation"] <- "ap_code"
names(dat)[names(dat) == "harbour_code"] <- "ap_code"
```

work on distance data
```{r}
dist <- dist_safe

names(dist)[names(dist) == "zipcode"] <- "zip_code"

#transform 5 digit zipcode to 2 digit
dist$zip_code <- formatC(dist$zip_code, width = 5, format = "d", flag = "0")
dist$zip_code <- as.numeric(substr(dist$zip_code, 1, 2))

dist <- subset(x = dist, select = c(ap_code, zip_code, distance_km))
```



combine both datasets with distances
```{r}
run <- left_join(run, dist)
names(run)[names(run) == "Angler_fishing_method"] <- "fishing_method"

dat <- left_join(dat, dist)
names(dat)[names(dat) == "survey_category"] <- "fishing_method"
```


calculate travel distances per zip code
```{r}
run <- run %>%
  group_by(fishing_method, zip_code) %>%
  summarise(min.dist.e = min(distance_km),
            max.dist.e = max(distance_km),
            mean.dist.e = mean(distance_km))
run <- mutate(.data = run, diff.e = max.dist.e - min.dist.e)

dat <- dat %>%
  group_by(fishing_method, zip_code) %>%
  summarise(min.dist = min(distance_km),
            max.dist = max(distance_km),
            mean.dist = mean(distance_km))
dat <- mutate(.data = dat, diff = max.dist - min.dist)
```

```{r}
test <- left_join(run, dat)

test <- test %>%
  group_by(fishing_method, zip_code)%>%
  mutate(min.diff = min.dist - min.dist.e,
          max.diff = max.dist - max.dist.e,
          mean.diff = mean.dist - mean.dist.e,
          diff.diff = diff - diff.e)

test %>%
  group_by(fishing_method)%>%
  summarise(mean = mean(abs(mean.diff)))
```

```{r}
dat_test <- filter(.data = dat, fishing_method == "Boot")
run_test <- filter(.data = run, fishing_method == "Boot")

ggplot()+
  geom_pointrange(data = dat_test, aes(x = mean.dist, y = zip_code, xmin = min.dist, xmax = max.dist))+
  geom_pointrange(data = run_test, aes(x = mean.dist.e, y = zip_code+0.5, xmin = min.dist.e, xmax = max.dist.e), col = "red")
```

```{r}
dat_test <- filter(.data = dat, fishing_method == "Kutter")
run_test <- filter(.data = run, fishing_method == "Kutter")

ggplot()+
  geom_pointrange(data = dat_test, aes(x = mean.dist, y = zip_code, xmin = min.dist, xmax = max.dist))+
  geom_pointrange(data = run_test, aes(x = mean.dist.e, y = zip_code+0.5, xmin = min.dist.e, xmax = max.dist.e), col = "red")
```


```{r}
dat_test <- filter(.data = dat, fishing_method == "Land")
run_test <- filter(.data = run, fishing_method == "Land")

ggplot()+
  geom_pointrange(data = dat_test, aes(x = mean.dist, y = zip_code, xmin = min.dist, xmax = max.dist))+
  geom_pointrange(data = run_test, aes(x = mean.dist.e, y = zip_code+0.5, xmin = min.dist.e, xmax = max.dist.e), col = "red")
```











# Conclusion

more fishing locations are visited in the real data than the experiment
& high average catch spots are overrepresented, low often not visited at all

-> means that the mechanismn to chose the location with the max average catch within the travel range send to many angler to the same spot and the bad spots are not visited often enough to represent the data








# Data for GIS

```{r}
run <- run_safe
names(run)[names(run) == "Angler_postcode"] <- "zip_code"
names(run)[names(run) == "FishingLocation"] <- "ap_code"

zip_ap <- zip_ap_safe
zip_ap <- subset(x = zip_ap, select = c(zipcode, long, lat, ap_code, longitude, latitude))
zip_ap$zipcode <- as.numeric(zip_ap$zipcode)
names(zip_ap)[names(zip_ap) == "zipcode"] <- "zip_code"
names(zip_ap)[names(zip_ap) == "long"] <- "zip_long"
names(zip_ap)[names(zip_ap) == "lat"] <- "zip_lat"
names(zip_ap)[names(zip_ap) == "longitude"] <- "ap_long"
names(zip_ap)[names(zip_ap) == "latitude"] <- "ap_lat"
```


```{r}
GIS_lv6 <- left_join(run, zip_ap)
```

```{r}
test <- sample_n(GIS_lv6, 1500)

#write.table(test, "GIS_lv6.csv", sep = ";", row.names = FALSE)
```






